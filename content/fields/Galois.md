+++
title = "Teoría de Galois"
weight = 20
+++

## El grupo de Galois

{{% definition %}}
Dada una extensión $F\subset K$, su **grupo de Galois** $G(K/F)$ es el conjunto de los automorfismos de $F\subset K$. 
{{% /definition %}}


{{% remark %}}
La operación de grupo del grupo de Galois es la composición de automorfismos. El elemento unidad es la identidad. El grupo de Galois de la extensión trivial es el grupo trivial $G(F/F)=\\{\operatorname{id}\_{F}\\}$. Recuerda que si la extensión $F\subset K$ es finita cualquier homomorfismo de extensiones $f\colon K\rightarrow K$ es un elemento de $G(K/F)$, y si además $F=\mathbb Q$ entonces cualquier homomorfismo de anillos $f\colon K\rightarrow K$ es un elemento de $G(K/F)$. Recuerda también que todo elemento de $G(K/F)$ es además un isomorfismo de $F$-espacios vectoriales, pero no todo isomorfismo de $F$-espacios vectoriales $f\colon K\rightarrow K$ está en $G(K/F)$ ya que podría no preservar el producto en $K$, o incluso el $1$. Asimismo, recuerda que todo elemento de $G(K/F)$ preserva raíces de polinomios con coeficientes en $F$. 
{{% /remark %}}


{{% example name="$G(\mathbb C/\mathbb R)$" %}}
Un homomorfismo de $\mathbb R$-espacios vectoriales $f\colon \mathbb C\rightarrow\mathbb C$ está determinado por la imagen de los elementos de una base, por ejemplo $\\{1,i\\}\subset\mathbb C$. Para que $f\in G(\mathbb C/\mathbb R)$ ha de ser un homomorfismo de anillos, así que debe satisfacer $f(1)=1$. También ha de preservar raíces en $\mathbb C$ de polinomios en $\mathbb R[ x ]$. Las raíces complejas de $x^2+1$ son $\pm i$, así que $f$ ha de cumplir $f(i)=\pm i$. Por tanto los dos posibles elementos de $G(\mathbb C/\mathbb R)$ son los homomorfismos de $\mathbb R$-espacios vectoriales definidos por

$$\begin{array}{rcl}f(1)&=&1,\cr f(i)&=&i,\end{array}$$

y por

$$\begin{array}{rcl}f(1)&=&1,\cr f(i)&=&-i.\end{array}$$

Algunos de estos dos homomorfismos de $\mathbb R$-espacios vectoriales podría no estar en $G(\mathbb C/\mathbb R)$ pues podría no preservar el producto, pero ambos lo preservan porque claramente el primero es la identidad $\operatorname{id}\_{\mathbb C}$ y el segundo es la conjugación, que denotaremos $c$. Así que $G(\mathbb C/\mathbb R)=\\{\operatorname{id}\_{\mathbb C},c\\}$, que es un grupo cíclico de orden $2$ generado por la conjugación $c$, que satisface $c\circ c=\operatorname{id}_{\mathbb C}$. 
{{% /example %}}

Si $F\subseteq K$ es una extensión finita, entonces $G(K/F)$ es un grupo finito: en efecto, $K$ está generado sobre $F$ por un número finito de elementos, y un automorfismo está completamente determinado por la imagen de dichos generadores. Como la imagen de cada uno de ellos debe ser una raíz de su polinomio mínimo, solo hay un número finito de posibilidades.

{{% proposition %}}
Si $car(F)\neq 2$ y $F\subset K$ es una extensión de grado $[K:F]=2$ entonces $K=F[\sqrt{\delta}]$ para cierto $\delta\in F$ y $G(K/F)=\\{\operatorname{id}_K,c\\}$ es un grupo cíclico de orden $2$ cuyo generador $c$ denominamos **conjugación** y está caracterizado por satisfacer $c(\sqrt{\delta})=-\sqrt{\delta}$. 
{{% /proposition %}}


{{% proof %}}
Como la extensión no es trivial, ha de existir algún $\alpha\in K$ tal que $\alpha\notin F$. El grado de este elemento ha de dividir a $2$. Como no puede ser $1$ porque $\alpha\notin F$, ha de ser $2$. La extensión $F\subset F[\alpha]$ también tiene grado $2$ y $F[\alpha]\subset K$ por tanto $K=F[\alpha]$. Si $x^2+ax+b\in F[ x ]$ es el polinomio mínimo de $\alpha$, entonces

$$\alpha=\frac{-a\pm\sqrt{\delta}}{2}$$

donde $\delta=a^2-4b\in F$ es el **discriminante**. Deducimos por tanto que $\sqrt{\delta}\in K$, $\sqrt{\delta}\notin F$, y $K=F[\sqrt{\delta}]$. Sabemos que $\\{1,\sqrt{\delta}\\}\subset K$ es una base como $F$-espacio vectorial. Como cualquier $f\in G(K/F)$ preserva el $1$ y las raíces de $x^2-\delta$, tenemos solo dos posibilidades:

$$\begin{array}{rcl}f(1)&=&1,\cr f(\sqrt{\delta})&=&\sqrt{\delta},\end{array}$$

y

$$\begin{array}{rcl}f(1)&=&1,\cr f(\sqrt{\delta})&=&-\sqrt{\delta}.\end{array}$$

El primero es la identidad $\operatorname{id}\_{K}$, que es obviamente un isomorfismo de extensiones. El segundo es el que denominamos conjugación $c$. Dejamos como ejercicio probar que la conjugación, que a priori es solo un homomorfismo de $F$-espacios vectoriales, es de hecho un homomorfismo de extensiones. Solo queda por demostrar que preserva el producto.
{{% /proof %}}


{{% example name="$G(\mathbb Q[\sqrt[3]{2}]/\mathbb Q)$" %}}
Aquí $\sqrt[3]{2}$ denota la raíz cúbica de $2$ real por lo que $\mathbb Q[\sqrt[3]{2}]\subset\mathbb R$. El resto de raíces cúbicas de $2$ son puramente complejas. Cualquier $f \in G(\mathbb Q[\sqrt[3]{2}]/\mathbb Q)$ ha de preservar las raíces de $x^3-2\in\mathbb Q[ x ]$. La única raíz de este polinomio que está en $\mathbb Q[\sqrt[3]{2}]$ es $\sqrt[3]{2}$, ya que las otras dos están en $\mathbb{C}\setminus\mathbb{R}$, por tanto $f(\sqrt[3]{2})=\sqrt[3]{2}$. Una base de $\mathbb Q[\sqrt[3]{2}]$ como $\mathbb Q$-espacio vectorial está formada por las primeras tres potencias de $\sqrt[3]{2}$, es decir, $\\{1,\sqrt[3]{2},(\sqrt[3]{2})^2\\}$. Como $f$ ha de preservar la unidad y los productos, $f$ tiene que mandar cada uno de los elementos de esta base a sí mismo, así que necesariamente $f=\operatorname{id}_{\mathbb Q[\sqrt[3]{2}]}$, por tanto en este caso el grupo de Galois es el tivial, $G(\mathbb Q[\sqrt[3]{2}]/\mathbb Q)=\\{\operatorname{id}\_{\mathbb Q[\sqrt[3]{2}]}\\}$ a pesar de que la extensión $\mathbb Q\subset \mathbb Q[\sqrt[3]{2}]$ no es trivial, es de grado $3$. 
{{% /example %}}

{{% proposition %}}
Dadas dos extensiones consecutivas $F\subset L\subset K$, tenemos que $G(K/L)\subset G(K/F)$. 
{{% /proposition %}}


{{% proof %}}
En efecto, si $f\colon K\rightarrow K$ es un isomorfismo de anillos que deja fijo a $L$ entonces también deja fijo a $F$ ya que $F\subset L$.
{{% /proof %}}


Los subgrupos del grupo de Galois nos permiten construir extensiones intermedias.

{{% definition %}}
Dada una extensión $F\subset K$ y un subgrupo $H\subset G(K/F)$ definimos el **cuerpo fijo** de $H$ del siguiente modo:

$$K^{H}=\\{\alpha\in K\mid f(\alpha)=\alpha\\;\forall f\in H\\}.$$

{{% /definition %}}


{{% proposition %}}
Dada una extensión $F\subset K$ y un subgrupo $H\subset G(K/F)$, el cuerpo fijo $K^H$ es un subcuerpo de $K$ que contiene a $F$,

$$F\subset K^H\subset K.$$

{{% /proposition %}}


{{% proof %}}
El conjunto $K^H$ está contenido en $K$ por definición. Es más, cualquier $\alpha\in F$ satisface $f(\alpha)=\alpha$ para todo $f\in G(K/F)$, en particular para todo $f\in H$, por tanto $F\subset K^H$.  

Veamos que $K^H\subset K$ es un subanillo. Obviamente $0,1\in F\subset K^H$. Si $\alpha,\beta\in K^H$ entonces, dado $f\in H$, como $f\colon K\rightarrow K$ es un homomorfismo de anillos,

$$\begin{array}{rcl}
f(\alpha+\beta)&=&f(\alpha)+f(\beta)\cr
&=&\alpha+\beta,\cr
f(-\alpha)&=&-f(\alpha)\cr
&=&-\alpha,\cr
f(\alpha\beta)&=&f(\alpha)f(\beta)\cr
&=&\alpha\beta.
\end{array}$$

Por tanto $\alpha+\beta,-\alpha,\alpha\beta\in K^H$. Además $K^H$ es un cuerpo porque si $\alpha\neq 0$ entonces

$$\begin{array}{rcl}
f(\alpha^{-1})&=&f(\alpha)^{-1}\cr
&=&\alpha^{-1}.
\end{array}$$

{{% /proof %}}

{{% proposition %}}
Dada una extensión $F\subset K$ y un subgrupo $H\subset G(K/F)$ tenemos que $H\subset G(K/K^H)$.
{{% /proposition %}}

{{% proof %}}
Es obvio porque, por definición de $K^H$, todos los automorfismos de la extensión que están en $H$ dejan fijo a $K^H$.
{{% /proof %}}


## Extensiones normales

{{% definition %}}
Una extensión $F\subset K$ se dice **normal** si es el cuerpo de descomposición de un polinomio mónico $p(x)\in F[x]$. 
{{% /definition %}}

{{% proposition %}}
Toda extensión $F\subset K$ de grado $2$ es normal. 
{{% /proposition %}}


{{% proof %}}
Sea $\alpha\in K\backslash F$, entonces $K=F[\alpha]$ (de lo contrario, $F[\alpha]$ sería una extensión intermedia de $F\subset K$, lo cual es imposible porque $2$ es primo). Si $p(x)=x^2+ax+b\in F[x]$ es el polinomio mínimo de $\alpha$, entonces las raíces de $p(x)$ son $\alpha$ y $-a-\alpha$, que están en $K$, por lo que $K$ contiene todas las raíces de $p(x)$ (y está generado por ellas) y es por tanto el cuerpo de descomposición de $p(x)$.
{{% /proof %}}

{{% proposition %}}
Sea $F\subset K$ es una extensión normal y $F\subset L\subset K$ una extensión intermedia. Entonces la extensión $L\subset K$ es normal. 
{{% /proposition %}}

{{% proof %}}
Si $K$ es el cuerpo de descomposición del polinomio $p(x)\in F[x]$, también es el cuerpo de descomposición del mismo polinomio visto en $L[x]$.
{{% /proof %}}

{{% proposition %}}
Sea $K \subset L$ una extensión finita. Las condiciones siguientes son equivalentes:
 
 1. La extensión $K\subset L$ es normal.
 2. Para todo $\alpha\in L$, el polinomio mínimo de $\alpha$ sobre $K$ tiene todas sus raíces en $L$ (es decir, se descompone en producto de factores lineales en $L[ x ]$).

{{% /proposition %}}
 
 {{% proof %}}
$\Rightarrow$) Supongamos que $L$ es el cuerpo de descomposición de $f\in K[x]$. Sea $\alpha\in L$ con polinomio mínimo $p\in K[x]$, y supongamos que $p$ no tiene todas sus raíces en $L$, es decir, tiene algún factor irreducible $q$ de grado $>1$ en $L$. Sea $L(\beta)$ una extensión de $L$ en la que $q$ tenga una raíz $\beta$. Entonces $L(\beta)$ es un cuerpo de descomposición de $f$ sobre $K(\beta)$.

 Por otra parte, tenemos un isomorfismo de extensiones de $K$, $\phi:K(\alpha)\to K(\beta)$, ya que $\alpha$ y $\beta$ tienen el mismo polinomio mínimo $p$ sobre $K$. Como $f$ tiene sus coeficientes en $K$, este isomorfismo deja $f$ invariante. Como se probó en el tema anterior, este isomorfismo se extiende a un isomorfismo entre los cuerpos de descomposición de $f$ sobre $K(\alpha)$ y sobre $K(\beta)$. Pero el cuerpo de descomposición de $f$ sobre $K(\alpha)$ es $L$, ya que $\alpha\in L$. Por tanto, tenemos un isomorfismo $L\to L(\beta)$, que además es $K$-lineal. Esto es imposible, ya que $L\subsetneq L(\beta)$ y por tanto tienen distinta dimensión sobre $K$.

$\Leftarrow$) Sea $L=K(\alpha_1,\ldots,\alpha_r)$, y $f_i\in K[x]$ el polinomio mínimo de $\alpha_i$. Entonces $L$ es el cuerpo de descomposición de $f_1f_2\cdots f_r$, ya que todas las raíces de cada uno de los $f_i$ están en $L$. 
{{% /proof %}}

{{% example %}}
La extensión $\mathbb Q\subset\mathbb Q[\sqrt[3]{2}]$ no es normal: en efecto, contiene a $\alpha=\sqrt[3]{2}$, pero no a las demás raíces del polinomio mínimo de $\alpha$, $x^3-2$ (que no son reales). Por tanto, no se cumple la condición (2) de la proposición.
{{% /example %}}


<!--## Funciones simétricas


{{% definition %}}
Dado un anillo $R$, un polinomio $f=f(u\_1,\dots,u\_n)\in R[u\_1,\dots,u\_n]$ y un elemento $\sigma\in S_n$. El polinomio $\sigma(f)\in R[u\_1,\dots,u\_n]$ es

$$
\sigma(f)=f(u\_{\sigma(1)},\dots, u\_{\sigma(n)}).
$$

La **órbita** de $f$ es el conjunto de polinomios

$$\\{\sigma(f)\mid \sigma\in S_n\\}.$$

Decimos que $f$ es **simétrico** si $f=\sigma(f)$ para todo $\sigma\in S_n$.
{{% /definition %}}

{{% example name="Una permutación aplicada a un polinomio" %}}
Si tomamos el polinomio $f=2u\_1^2u\_3^2-3u\_2\in\mathbb{Z}[u\_1,u\_2,u\_3]$ y le aplicamos la permutación

$$\sigma=\left(
\begin{array}{ccc}
1&2&3\cr
2&3&1
\end{array}
\right)=(1\\;2\\;3)\in S_3$$

obtenemos el polinomio 

$$\sigma(f)=2u\_2^2u\_1^2-3u\_3.$$

Considerando las $3!=6$ permutaciones de $S_3$, puedes comprobar que la órbita de $f$ es el conjunto

$$
\\{2u\_1^2u\_3^2-3u\_2, 2u\_2^2u\_3^2-3u\_1, 2u\_1^2u\_2^2-3u\_3\\}.
$$

{{% /example %}}

{{% remark %}}
La órbita de un polinomio en $n$ variables tiene como máximo $|S\_n|=n!$ elementos. Es más, el número de elementos de la órbita divide a $n!$. La órbita de un polinomio es un conjunto unitario si y solo si es simétrico. Los polinomios simétricos forman un subanillo de $R[u\_1,\dots,u\_n]$. La aplicación

$$\sigma\colon R[u\_1,\dots,u\_n]\rightarrow R[u\_1,\dots,u\_n]$$

definida arriba es, por el principio de sustitución, el único homomorfismo de anillos tal que $\sigma\_{|\_R}$ es la inclusión $R\subset R[u\_1,\dots,u\_n]$ y $\sigma(u\_i)=u\_{\sigma(i)}$. Dadas $\sigma,\tau\in S_n$ y $f\in R[u\_1,\dots,u\_n]$,

$$\sigma(\tau(f))=(\sigma\tau)(f),$$

por tanto el producto de permutaciones se corresponde con la composición de los homomorfismos inducidos. En particular, estos últimos son automorfismos ya que el inverso de $\sigma$ será el definido por la permutación inversa $\sigma^{-1}$. Aquí usamos que la permutación identidad induce la identidad. 
{{% /remark %}}


{{% definition %}}
Los **polinomios simétricos** o **funciones simétricas elementales** en $n$ variables $s\_i\in R[u\_1,\dots,u\_n]$ son:

$$
\begin{array}{rcl}
s\_1&=&\displaystyle \sum\_{1\leq i\leq n}u\_i=u\_1+u\_2+\cdots+u\_n,\cr
s\_2&=&\displaystyle \sum\_{1\leq i < j\leq n}u\_iu\_j=u\_1u\_2+u\_1u\_3+\cdots+u\_{n-1}u\_n,\cr
s\_3&=&\displaystyle \sum\_{1\leq i < j < k\leq n}u\_iu\_ju\_k=u\_1u\_2u\_3+\cdots+u\_{n-2}u\_{n-1}u\_n,\cr
&\vdots&\cr
s\_n&=&u\_1\cdots u\_n.
\end{array}
$$

Es decir, para cada $1\leq j\leq n$,

$$
s\_j=\sum_{1\leq i\_1<\cdots<i\_j\leq n}u\_{i\_1}\cdots u\_{i\_j}.
$$

{{% /definition %}}

{{% remark %}}
Las funciones simétricas elementales en $n$ variables son, salvo signo, los coeficientes del polinomio

$$
\begin{array}{rcl}
P(x)&=&(x-u\_1)\cdots (x-u\_n)\cr
&=&x^n-s\_1x^{n-1}+s\_2x^{n-2}-\cdots+(-1)^ns\_n\cr
&=&\displaystyle \sum\_{i=0}^n(-1)^is\_ix^{n-i}.
\end{array}
$$

En la última línea denotamos $s_0=1$.

En particular, dado un polinomio mónico $f\in F[ x ]$ de grado $n$ 

$$
\begin{array}{rcl}
P(x)&=&x^n-a\_1x^{n-1}+a\_2x^{n-2}-\cdots+(-1)^na\_n\cr
&=&\displaystyle \sum\_{i=0}^n(-1)^ia\_ix^{n-i}\cr
&=&(x-\alpha\_1)\cdots (x-\alpha\_n)
\end{array}
$$

con $a\_0=1$, cuyas $n$ raíces complejas denotamos $\alpha\_i$, sus coeficientes se obtienen al aplicarle las funciones simétricas elementales a estas raíces,

$$a\_i=s\_i(\alpha\_1,\dots,\alpha\_n).$$

{{% /remark %}}



{{% theorem name="de las funciones simétricas" %}}
Dado un polinomio simétrico $g\in R[u\_1,\dots,u\_n]$, existe un único polinomio $G\in R[z\_1,\dots,z\_n]$ tal que $g=G(s\_1,\dots,s\_n)$.
{{% /theorem %}}

{{% proof %}}
Por doble inducción, primero en el número de variables y luego en el grado. 

Para una sola variable, el resultado es obviamente cierto para cualquier grado ya que $s\_1=u\_1$ y basta tomar $G=g(z\_1)$. También es obvio para polinomios de grado $0$. Supongamos que es cierto para polinomios de hasta $n-1$ variables de cualquier grado y para polinomios de $n$ variables de grado $< \operatorname{grado} g$. 

Consideramos el polinomio $g_0=g(u\_1,\dots,u\_{n-1},0)$. Por hipótesis de inducción existe $G_0\in R[z\_1,\dots,z\_{n-1}]$ tal que $g\_0=G\_0(s'\_1,\dots,s'\_{n-1})$, donde las $s\_i'\in R[u\_1,\dots,u\_{n-1}]$ son las funciones simétricas elementales en $n-1$ variables. El polinomio 

$$p(u\_1,\dots,u\_n)=g(u\_1,\dots,u\_n)-G_0(s\_1,\dots,s\_{n-1})$$

es simétrico pues los polinomios simétricos forman un subanillo de $R[u\_1,\dots,u\_n]$. Por construcción, 

$$p(u\_1,\dots,u\_{n-1},0)=g\_0-G\_0(s'\_1,\dots,s'\_{n-1})=0$$

así que $u\_n|p$. Como $p$ es simétrico, esto implica que $u\_i|p$ para todo $1\leq i\leq n$, así que $s\_n=u\_1\cdots u\_n|p$. Ha de existir por tanto un polinomio $h\in R[u\_1,\dots,u\_n]$ tal que $p=s\_nh$. Al ser $p$ y $s\_n$ simétricos, $h$ es también simétrico. Como $h$ es de menor grado que $g$, por hipótesis de inducción existe $H\in R[z\_1,\dots,z\_n]$ tal que $h=H(s\_1,\dots,s\_n)$. Al ser 

$$
\begin{array}{rcl}
g&=&p+G  _0(s\_1,\dots,s\_{n-1})\cr
&=&s\_nH(s\_1,\dots,s\_n)+G_0(s\_1,\dots,s\_{n-1})
\end{array}
$$

podemos tomar $G=z\_nH+G\_0$.
{{% /proof %}}

{{% definition %}}
El **discriminante** en $n$ variables es el polinomio 

$$D=\prod_{1\leq i<j\leq n}(u\_i-u\_j)^2=(u\_1-u\_2)^2\cdots(u\_{n-1}-u\_n)^2.$$

{{% /definition %}}

{{% remark %}}
El discriminante es simétrico y, dados $\alpha\_1, \dots,\alpha_n$, tenemos que $D(\alpha\_1, \dots,\alpha_n)=0$ si y solo si $\alpha\_i=\alpha\_j$ para ciertos $i\neq j$. Denotaremos $\Delta\in R[z\_1,\dots,z\_n]$ al único polinomio tal que $D=\Delta(s\_1,\dots,s\_n)$.
{{% /remark %}}

{{% example name="Discriminantes en pocas variables" %}}
Para $n=1$ el discriminante es $D=1$. Si $n=2$, entonces

$$
\begin{array}{rcl}
D&=&(u\_1-u\_2)^2\cr
&=&(u\_1+u\_2)^2-4u\_1u\_2\cr
&=&s\_1^2-4s\_2.
\end{array}
$$

Recuerda que el discriminante de un polinomio de grado $2$

$$x^2-a\_1x+a\_2=(x-\alpha\_1)(x-\alpha\_2)$$

es

$$a\_1^2-4a\_2=\Delta(a\_1,a\_2)=D(\alpha\_1,\alpha\_2).$$

{{% /example  %}}

{{% lemma %}}
Dado $p\_1\in R[u\_1,\dots,u\_n]$, si $\\{p\_1,\dots,p\_l\\}$ es su órbita y $h\in R[w\_1,\dots,w\_l]$ es simétrico entonces $h(p\_1,\dots,p\_l)\in R[u\_1,\dots,u\_n]$ también es simétrico.
{{% /lemma %}}

{{% proof %}}
Tomemos $\tau\in S_n$. Como la órbita es

$$S=\\{\sigma(p\_1) \mid \sigma\in S_n\\}$$

y $\tau(\sigma(p\_i))=(\tau\sigma)(p\_1)\in S$, deducimos que $\tau(S)\subset S$. Es más, como $\tau\colon R[u\_1,\dots,u\_n]\rightarrow R[u\_1,\dots,u\_n]$ es un automorfismo, $\tau\_{|\_S}$ es una permutación de $S$. Por tanto, ha de existir $\tau'\in S_l$ tal que

$$\tau(p\_i)=p\_{\tau'(i)}$$

para todo $i$. Entonces tenemos que 

$$
\begin{array}{rcl}
\tau(h(p\_1,\dots,p\_l))&=&h(\tau(p\_1),\dots,\tau(p\_l))\cr
&=&h(p\_{\tau'(1)},\dots,p\_{\tau'(l)})\cr
&=&\tau'(h)(p\_1,\dots,p\_l)\cr
&=&h(p\_1,\dots,p\_l)
\end{array}
$$

por ser $h$ simétrica.
{{% /proof %}}

{{% theorem name="de descomposición" %}}
Si $K$ es el cuerpo de descomposición de $f\in F[ x ]$ y $g\in F[ x ]$ es mónico e irreducible y posee una raíz en $K$ entonces todas las raíces complejas de $g$ están en $K$.
{{% /theorem %}}

{{% proof %}}
Sean $\alpha\_1,\dots,\alpha\_n$ las raíces complejas de $f$ y $\beta\_1$ la raíz de $g$ que está en $K$. Como $\beta\_1\in K=F[\alpha\_1,\dots,\alpha\_n]$, existe $p\_1\in F[u\_1,\dots,u\_n]$ tal que $\beta\_1=p\_1(\alpha\_1,\dots,\alpha\_n)$. Sea $\\{p\_1,\dots,p\_l\\}$ la órbita de $p\_1$ y $\beta\_i=p\_i(\alpha\_1,\dots,\alpha\_n)\in K$, $1\leq i\leq l$. 

Nuestro objetivo ahora es probar que las raíces complejas de $g$ están entre los $\beta\_1,\dots,\beta\_l\in K$. Para ello consideramos el polinomio 

$$h(x)=(x-\beta\_1)\cdots (x-\beta\_l).$$

Supongamos que hemos probado que $h$ tiene coeficientes en $F$. Como $g$ es el polinomio mínimo de $\beta\_1$ sobre $F$ y $\beta\_1$ también es raíz de $h$, deduciremos que $g|h$ en $F[ x ]$, así que las raíces de $g$ están entre las de $h$, que es lo que nos habíamos propuesto demostrar.

Para ver que $h$ tiene coeficientes en $F$, tomamos las funciones simétricas elementales $s'\_1,\dots, s'\_l$ en $l$ nuevas variables $w\_1,\dots,w\_l$. Los coeficientes de $h$ son los 

$$s'\_i(\beta\_1,\dots,\beta\_l)=s'\_i(p\_1(\alpha\_1,\dots,\alpha\_n),\dots,p\_l(\alpha\_1,\dots,\alpha\_n)).$$

Los polinomios $s'\_i(p\_1,\dots,p\_l)\in F[u\_1,\dots,u\_n]$ son simétricos en las $n$ variables $u\_1,\dots,u\_n$ por el lema anterior. Por el teorema de las funciones simétricas, existen $G\_1,\dots, G\_l\in F[z\_1,\dots,z\_n]$ tales que $G\_i(s\_1,\dots,s\_n)=s'\_i(p\_1,\dots,p\_l)$. Aquí $s\_i\in F[u\_1,\dots,u\_n]$ son las funciones simétricas en las $n$ variables $u\_1,\dots,u\_n$. Así que los coeficientes de $h$ son

$$
G\_i(s\_1(\alpha\_1,\dots,\alpha\_n),\dots,s\_n(\alpha\_1,\dots,\alpha\_n)).
$$

Sabemos que $s\_i(\alpha\_1,\dots,\alpha\_n)\in F$ pues son los coeficientes de $f$. Como los $G\_i$ también tiene coeficientes en $F$, deducimos de la fórmula anterior que los coeficientes de $h$ están en $F$.
{{% /proof %}}-->

## Órbitas 

{{% definition %}}
Dada una extensión $F\subset K$ y un subgrupo $H\subset G(K/F)$, la **órbita** de $\alpha\in K$ por $H$ es 

$$\\{\sigma(\alpha)\mid \sigma\in H\\}.$$

{{% /definition %}}

{{% theorem name="de la órbita" %}}
Dada una extensión $F\subset K$ y un subgrupo $H\subset G(K/F)$, $\beta_1\in K$ es algebraico sobre $K^H$ si y solo si la órbita de $\beta_1$ por $H$ es finita. En dicho caso, si la órbita es $\\{\beta\_1,\dots,\beta\_l\\}$, el polinomio mínimo de $\beta\_1$ sobre $K^H$ es

$$g(x)=(x-\beta\_1)\cdots (x-\beta\_l).$$

En particular el grado de $\beta_1$ sobre $K^H$ es el número de elementos de su órbita.
{{% /theorem %}}

{{% proof %}}
$\Leftarrow$ Cada $f\in H$ induce una permutación de $\\{\beta\_1,\dots,\beta\_l\\}$. Los coeficientes de $g$ son funciones simétricas elementales evaluadas en los $\beta\_i$, por tanto no varían al aplicar $f\in H$. Esto demuestra que estos coeficientes están en $K^H$, por tanto $\beta_1$ es algebraico sobre $K^H$.

$\Rightarrow$ Sea $h\in K^H[ x ]$ un polinomio que tenga $\beta\_1$ como raíz. Todo elemento de $f\in H\subset G(K/K^H)$ envía raíces de un polinomio con coeficientes en $K^H$ en otras raíces, por tanto toda la órbita de $\beta_1$ por $H$ está formada por raíces de $h$. Como todo polinomio con coeficientes en un cuerpo tiene una cantidad finita de raíces, deducimos que la órbita es finita.  

Hemos visto que cuando la órbita es finita el polinomio $g$ tiene coeficientes en $K^H$ y que todos los elementos de la órbita son también raíces de cualquier otro polinomio $h\in K^H[ x ]$ que tenga $\beta\_1$ como raíz. Esto prueba que $g|h$ en $K[ x ]$ y por tanto también en $K^H[ x ]$, así que efectivamente $g$ es el polinomio mínimo de $\beta_1$ sobre $K^H$.
{{% /proof %}}

{{% corollary %}}
Bajo las condiciones del teorema de la órbita, si la extensión $F\subset K$ es finita, entonces la extensión $K^H\subset K$ es separable.
{{% /corollary %}}

{{% proof %}}
El teorema de la órbita muestra que el polinomio mínimo sobre $K^H$ de cualquier elemento $\beta\in K$ es separable.
{{% /proof %}}

<!--{{% definition %}}
Una extensión $F\subset K$ es **algebraica** si todo elemento de $K$ es algebraico sobre $F$.
{{% /definition %}}

Hemos visto que las extensiones finitas son algebraicas. El recíproco no es cierto en general, pero sí bajo ciertas hipótesis.

{{% lemma %}}
Si $F\subset K$ es una extensión algebraica y el grado de los elementos de $K$ sobre $F$ está uniformemente acotado entonces la extensión es finita.
{{% /lemma %}}

{{% proof %}}
Vamos a probar que si no fuera finita entonces existirían elementos de grado arbitrariamente grande. Para ello construimos una sucesión estrictamente creciente de extensiones intermedias

$$F=F\_0\subsetneq F\_1\subsetneq F\_2\subsetneq\cdots\subsetneq K$$

tales que $F\_{i-1}\subsetneq F\_i$ es finita del siguiente modo. Supuesto construido hasta $F\_{n-1}$, tomamos un elemento $\alpha\_n\in K\setminus F\_{n-1}$ y definimos $F\_n=F\_{n-1}[\alpha\_n]$. Como $\alpha\_n$ es algebraico sobre $F$, también lo es sobre $F\_{n-1}$, así que $F\_{n-1}\subsetneq F\_n$ es finita, y en consecuencia $F\subsetneq F\_n$ también, así que $F\_n\subsetneq K$. Por la fórmula del grado para extensiones intermedias $[F\_n:F]\geq 2^n$, así que cualquier elemento primitivo de $F\subsetneq F\_n$ tiene grado $\geq 2^n$.
{{% /proof %}}-->

{{% theorem name="del cuerpo fijo" %}}
Sea $F\subset K$ una extensión finita y $H\subset G(K/F)$ un subgrupo. Entonces $[K:K^H]=|H|$.
{{% /theorem %}}

{{% proof %}}
Por el corolario anterior, la extensión $K^H\subset K$ es separable por lo que, aplicando teorema del elemento primitivo, existe $\gamma\in K$ tal que $K=K^H[\gamma]$. Un elemento $\sigma$ de $H$ actúa trivialmente sobre $K^H$, así que está unívocamente determinado por $\sigma(\gamma)$, y por tanto la órbita de $\gamma$ por $H$ tiene exactamente $|H|$ elementos. Por el teorema de la órbita, el grado de $\gamma$ sobre $K^H$ es $|H|$, y este grado es $[K:K^H]$.
{{% /proof %}}

{{% corollary %}}
Si $F\subset K$ es una extensión finita entonces $|G(K/F)|$ divide a $[K:F]$.
{{% /corollary %}}

{{% proof %}}
Por el teorema del cuerpo fijo, $|G(K/F)|=[K:K^{G(K/F)}]$, y por la fórmula del grado para extensiones intermedias este último número divide a $[K:F]$.
{{% /proof %}}

## Extensiones de Galois

{{% definition %}}
Una extensión finita $F\subset K$ es de **Galois** si es normal y separable.
{{% /definition %}}

En particular, si $F$ tiene característica cero, ser normal y ser de Galois es equivalente. Si la extensión $F\subset K$ es de Galois y $F\subset L \subset K$ es una extensión intermedia, entonces $L\subset K$ es de Galois (ya hemos visto, independientemente, que es normal y separable).

<!--{{% lemma %}}
Sea $F\subset K=F[\gamma\_1]$ una extensión finita, $g\in F[ x ]$ es el polinomio mínimo de $\gamma\_1$ y $\gamma\_1,\dots,\gamma\_r\in K$ las distintas raíces de $g$ en este cuerpo. Para cada $1\leq i\leq r$ existe un único $f\_i\in G(K/F)$ tal que $f\_i(\gamma\_1)=\gamma\_i$. Es más, $G(K/F)=\\{f\_1,\dots,f\_r\\}$.
{{% /lemma %}}

{{% proof %}}
Todos los $\gamma\_i$ poseen el mismo grado sobre $F$ ya que tienen el mismo polinomio mínimo $g$, por tanto $K=F[\gamma\_i]$ para todo $i$. Sabemos que, para cada $i$, hay un único isomorfismo

$$h\_i\colon \frac{F[ x ]}{(g)}\cong K$$

que deja fijo a $F$ tal que $h(\bar{x})=\gamma\_i$. Por tanto, $f\_i=h\_ih\_1^{-1}\in G(K/F)$ es el único que satisface la propiedad del enunciado. Todo elemento $f\in G(K/F)$  está determinado por $f(\gamma\_1)$ y además preserva raíces de $g\in F[ x ]$, así que $G(K/F)$ consta necesariamente de los $f\_i$ anteriores.
{{% /proof %}}-->

{{% theorem %}}
Dada una extensión finita $F\subset K$, los siguientes enunciados son equivalentes:

1. $F\subset K$ es de Galois.
2. $F=K^{G(K/F)}$.
3. $[K:F]=|G(K/F)|$.

{{% /theorem %}}

{{% proof %}}
$2\Leftrightarrow 3$ se sigue inmediatamente del teorema del cuerpo fijo, ya que siempre se tiene la inclusión $F\subset K^{G(K/F)}\subset K$ y, por tanto, $F=K^{G(K/F)}$ si y solo si $[K:F]=[K:K^{G(K/F)}]=|G(K/F)|$.

Veamos $2\Rightarrow 1$. Por el colorario al teorema de la órbita, la extensión $F=K^{G(K/F)}\subset K$ es separable, falta probar que es normal. Sea $\alpha\in K$, y $f\in F[ x ]$ su polinomio mínimo, hay que probar que todas las raíces de $f$ están en $K$. Sean $\alpha=\alpha_1,\ldots,\alpha_r$ las raíces de $f$ en $K$, y $g(x)=(x-\alpha_1)\cdots(x-\alpha_r)$. Claramente $g(x)|f(x)$. Como los elementos de $G(K/F)$ permutan las raíces de $g$, el polinomio $g$ es invariante por la acción de $G(K/F)$, es decir, está en $K^{G(K/F)}[ x ]=F[ x ]$. Y como tiene a $\alpha$ como raíz, debe ser múltiplo del polinomio mínimo, es decir, $f(x)|g(x)$. Por tanto, $g(x)=f(x)$ y $f$ no tiene más raíces aparte de $\alpha_1,\ldots,\alpha_r$, que están en $L$.

Veamos $1\Rightarrow 2$. La extensión $F\subset K$ es normal, y por tanto el cuerpo de descomposición de un polinomio $p\in F[ x ]$. Supongamos que la inclusión $F\subset K^{G(K/F)}$ fuera estricta, y sea $\alpha\in K^{G(K/F)}\backslash F$. Como $\alpha\not\in F$, su polinomio mínimo tiene grado mayor que $1$, y por ser separable tiene al menos otra raíz $\beta$, que también está en $K$ por ser la extensión $F\subset K$ normal. Como $\alpha$ y $\beta$ tienen el mismo polinomio mínimo, existe un isomorfismo de extensiones de $F$, $\phi:F[\alpha]\to F[\beta]$ que envía $\alpha$ en $\beta$. Notemos que $K$ es el cuerpo de descomposición de $p$ sobre $F[\alpha]$ y sobre $F[\beta]$. Por el teorema de extensión de isomorfismos, existe un automorfismo $K\to K$ que restringido a $F[\alpha]$ coincide con $\phi$. En particular, este automorfismo envía $\alpha$ en $\beta\neq\alpha$, lo cual contradice el hecho de que $\alpha\in K^{G(K/F)}$.
{{% /proof %}}

Extraemos de la prueba anterior el siguiente resultado, que será útil más adelante:
{{% corollary %}}
Dada una extensión de Galois $F\subset K$ y dos elementos $\alpha,\beta\in K$ conjugados sobre $F$, existe un automorfismo $\sigma\in G(K/F)$ tal que $\sigma(\alpha)=\beta$.
{{% /corollary %}}


{{% corollary %}}
Dada una extensión finita $F\subset K$ y un subgrupo $H\subset G(K/F)$, la extensión $K^H\subset K$ es de Galois y $H=G(K/K^H)$.
{{% /corollary %}}

{{% proof %}}
Sabemos que, en general, $H\subset G(K/K^H)$ es un subgrupo, así que $|H|\leq |G(K/K^H)|$. También sabemos que $|G(K/K^H)|$ divide a $[K:K^H]=|H|$, así que tenemos también la otra desigualdad $|G(K/K^H)|\leq|H|$. Esto prueba que $H=G(K/K^H)$, por tanto esta extensión es de Galois.
{{% /proof %}}

{{% corollary %}}
Toda extensión finita separable $F\subset K$ es una extensión intermedia $F\subset K\subset L$ de una extensión de Galois $F\subset L$.
{{% /corollary %}}

{{% proof %}}
Sea $\beta\in K$ un elemento primitivo de la extensión, y $p(x)\in F[x]$ su polinomio mínimo, que es separable. Tomemos como $L$ un cuerpo de descomposición de $p(x)$ sobre $K$ (que lo es también sobre $F$). La extensión $F\subset L$ es claramente normal, falta probar que es separable. Veamos, por inducción sobre el grado, que una extensión $F\subset L$ que sea el cuerpo de descomposición de un polinomio separable es una extensión de Galois (y, en particular, separable).

Por el teorema, basta probar que $|G(L/F)|=[L:F]$. Esto es obvio si $[L:F]=1$, así que supongamos que $[L:F]>1$, siendo $L$ el cuerpo de descomposición de un polinomio separable $p(x)$ sobre $F$. Sea $\alpha\in L$ una raíz de $p(x)$ que no esté en $F$, $q(x)$ su polinomio mínimo sobre $F$ (que debe dividir a $p(x)$, y por tanto es separable), y denotemos por $d$ su grado. Como la extensión es normal, las $d$ raíces de $q(x)$ están en $L$. Razonando como en la prueba del apartado $1\Rightarrow 2$ del teorema, existe un automorfismo de la extensión $F\subset L$ que lleva $\alpha$ en cada una de las $d$ raíces de $q(x)$. Además, componiendo un tal automorfismo con un elemento de $G(L/F(\alpha))$ (que fija $\alpha$) obtenemos un automorfismo que actúa igual sobre $\alpha$, por lo que existen (al menos) $|G(L/F(\alpha))|$ automorfismos que llevan $\alpha$ en cada una de las $d$ raíces de $q(x)$. Por tanto,

$$|G(L/F)|\geq d\cdot |G(L/F(\alpha))|.$$

Por hipótesis de inducción, $|G(L/F(\alpha))|=[L:F(\alpha)]$ (ya que $L$ es también el cuerpo de descomposición de $p(x)$ sobre $F(\alpha)$), de donde

$$|G(L/F)|\geq d\cdot [L:F(\alpha)] = [F(\alpha):F]\cdot [L:F(\alpha)] = [L:F]$$

y por tanto se tiene la igualdad, es decir, $F\subset L$ es de Galois (ya que la desigualdad contraria se tiene siempre).
{{% /proof %}}

{{% theorem name="fundamental de la teoría de Galois" %}}
Dada una extensión de Galois $F\subset K$, las siguientes aplicaciones son biyectivas y mutuamente inversas:

$$\begin{array}{rcl}\left\\{\text{ext. intermedias }F\subset L\subset K\right\\}&\longleftrightarrow& \left\\{\text{subgrupos }H\subset G(K/F)\right\\},\cr L&\mapsto&G(K/L),\cr K^H&\leftarrow&H.\end{array}$$

{{% /theorem %}}

{{% proof %}}
Dado un subgrupo $H\subset G(K/F)$, ya hemos probado en un corolario anterior que $H=G(K/K^H)$, así que la composición que empieza y acaba en la derecha es la identidad. Dada ahora una extensión intermedia $F\subset L\subset K$, sabemos que $L\subset K$ es de Galois, así que por el teorema anterior $K^{G(K/L)}=L$.
{{% /proof %}}

{{% remark %}}
Observa que la correspondencia dada en el Teorema Fundamental da la vuelta a las inclusiones. Es decir, dados dos subgrupos $H'\subset H\subset G(K/F)$ tenemos que $K^{H'}\supset K^H$ y dadas extensiones intermedias $F\subset L\subset L'\subset K$ tenemos que $G(K/L)\supset G(K/L')$. El subgrupo trivial se corresponde con $K$ y el total con $F$.
{{% /remark %}}

{{% corollary %}}
Toda extensión finita separable $F\subset K$ posee una cantidad finita de extensiones intermedias.
{{% /corollary %}}

{{% proof %}}
Cuando la extensión es de Galois el resultado es cierto porque el grupo $G(K/F)$, que es finito, tiene una cantidad finita de subgrupos, que se corresponden con las extensiones intermedias. Si $F\subset K$ no fuera de Galois, basta tomar $F\subset K\subset L$ con $F\subset L$ de Galois y observar que toda extensión intermedia de $F\subset K$ lo es también de $F\subset L$.
{{% /proof %}}

{{% theorem %}}
Dada una extensión de Galois $F\subset K$ y una extensión intermedia $F\subset L\subset K$, $F\subset L$ es de Galois si y solo si el subgrupo $G(K/L)\subset G(K/F)$ es normal. En dicho caso

$$\frac{G(K/F)}{G(K/L)}\cong G(L/F).$$

{{% /theorem %}}

{{% proof %}}
Comenzaremos probando la equivalencia de la primera parte del enunciado.

$\Rightarrow$  Sea $h\in G(K/L)$ un elemento cualquiera. Para ver que este subgrupo es normal tenemos que probar que, para todo $f\in G(K/F)$, $f^{-1}hf$ está en $G(K/L)$, es decir, que deja fijo a todo elemento de $L$. Sea $\alpha\in L$, entonces $f(\alpha)$ es un conjugado de $\alpha$. Como la extensión $F\subseteq L$ es normal, $f(\alpha)\in L$. Por tanto, $h(f(\alpha))=f(\alpha)$ o, equivalentemente, $(f^{-1}hf)(\alpha)=f^{-1}(h(f(\alpha)))=\alpha$.

$\Leftarrow$ Por el teorema de caracterización de las extensiones normales, debemos probar que para todo $\alpha\in L$ el polinomio mínimo $q(x)$ de $\alpha$ sobre $F$ se descompone en factores lineales en $L[x]$. Como la extensión $F\subseteq K$ es normal, $q(x)$ se descompone en $K[x]$ como $(x-\alpha\_1)\cdots(x-\alpha\_n)$, y hay que probar que $\alpha_i\in L$ para todo $i$. Como $\alpha$ y $\alpha\_i$ son conjugados sobre $F$, existe un automorfismo $f\in G(K/F)$ tal que $f(\alpha)=\alpha\_i$.

Sea $h\in G(K/L)$, entonces $h(\alpha\_i)=h(f(\alpha))=f(f^{-1}hf)(\alpha)=f(\alpha)=\alpha\_i$, ya que $f^{-1}hf$ está en $G(K/L)$ (y, por tanto, fija $\alpha\in L$) por ser éste un subgrupo normal. Es decir, $\alpha\_i$ está en el cuerpo fijo de $G(K/L)$, que es $L$.

Una vez establecida la equivalencia de la primera parte del enunciado, demostraremos el isomorfismo de la segunda. Supongamos pues que $F\subset L$ es de Galois. Hemos visto que entonces todo $f\in G(K/F)$ se restringe a $L$, es decir $f\_{|\_{L}}\in G(L/F)$. Esta restricción induce un homomorfismo de grupos

$$
\begin{array}{rcl}
\Phi:G(K/F)&\longrightarrow&G(L/F),\cr
f&\mapsto&f\_{|\_{L}}.
\end{array}
$$

cuyo núcleo está formado por los automorfismos de $K$ que actúan trivialmente en $L$, es decir, $G(K/L)$. El teorema de isomorfía nos da entonces un isomorfismo entre el cociente $G(K/F)/G(K/L)$ y la imagen de $\Phi$, que es un subgrupo de $G(L/F)$. Pero esta imagen tiene orden $|G(K/F)|/|G(K/L)|=[K:F]/[K:L]=[L:F]=|G(L/F)|$, y por tanto es el grupo $G(L/F)$ entero.

{{% /proof %}}

## El grupo de Galois como grupo de permutaciones

El **grupo simétrico** de $n$ letras, es decir el **grupo de permutaciones** de $\\{1,\dots,n\\}$, se denotará $S\_n$. 

{{% proposition %}}
Dada una extensión $F\subset K$, si $K$ es el cuerpo de descomposición de un polinomio $p(x)\in F[ x ]$ con $n$ raíces distintas, denotadas $\alpha\_1,\dots,\alpha\_n$, en $K$, entonces hay un único homomorfismo

$$\varphi\colon G(K/F)\longrightarrow S\_n$$

tal que, para un $f\in G(K/F)$, la permutación $\varphi(f)=\sigma$ es la única que satisface la siguiente ecuación para todo $i=1,\dots,n$,

$$f(\alpha\_i)=\alpha_{\sigma(i)}.$$

Además, este homomorfismo es inyectivo.
{{% /proposition %}}


{{% proof %}}
Como $f$ preserva raíces de polinomios con coeficientes en $F$, $f$ ha de mandar el conjunto $\\{\alpha\_1,\dots,\alpha\_n\\}$ dentro de sí mismo. Además ha de hacerlo de manera biyectiva por ser $f$ un automorfismo, por tanto existe una única permutación $\sigma\in S\_n$ que satisface la ecuación del enunciado. Esto me permite definir la aplicación $\varphi$ de manera única. 

Veamos que $\varphi$ es un homomorfismo de grupos. Por un lado $\varphi(\operatorname{id}\_{K})$ es la permutación identidad ya que $\operatorname{id}\_{K}(\alpha\_i)=\alpha\_i$. Por otro lado, dados $f,g\in G(K/F)$, si denotamos $\varphi(f)=\sigma$ y $\varphi(g)=\tau$ entonces 

$$\begin{array}{rcl}
(f\circ g)(\alpha\_i)&=&f(g(\alpha\_i))\cr
&=&f(\alpha\_{\tau(i)})\cr
&=&\alpha\_{\sigma(\tau(i))}\cr
&=&\alpha\_{(\sigma\circ\tau)(i)}.
\end{array}$$

Por tanto

$$\begin{array}{rcl}
\varphi(f\circ g)&=&\sigma\circ\tau\cr
&=&\varphi(f)\circ \varphi(g).
\end{array}$$

Comprobemos por último que $\varphi$ es inyectivo. Para ello debemos probar que si $f\in G(K/F)$ es tal que $\varphi(f)$ es la permutación identidad entonces $f=\operatorname{id}\_{K}$. Por definición $\varphi(f)$ es la permutación identidad si y solo si $f(\alpha\_i)=\alpha\_i$ para todo $i=1,\dots,n$. Como $K=F[\alpha\_1,\dots,\alpha\_n]$, todo elemento $\alpha\in K$ se puede escribir como polinomio en $\alpha\_1,\dots,\alpha\_n$ con coeficientes en $F$, es decir

$$\alpha=\sum\_{m\_i\geq 0} b\_{m\_1,\dots,m\_n}\alpha\_1^{m\_1}\cdots\alpha\_n^{m\_n}$$

para ciertos $b\_{m\_1,\dots,m\_n}\in F$, casi todos nulos (y no necesariamente únicos, pero esto es irrelevante). Entonces

$$\begin{array}{rcl}
f(\alpha)&=&f\left(\sum\_{m\_i\geq 0} b\_{m\_1,\dots,m\_n}\alpha\_1^{m\_1}\cdots\alpha\_n^{m\_n}\right)\cr
&=&\sum\_{m\_i\geq 0} f(b\_{m\_1,\dots,m\_n}\alpha\_1^{m\_1}\cdots\alpha\_n^{m\_n})\cr
&=&\sum\_{m\_i\geq 0} f(b\_{m\_1,\dots,m\_n})f(\alpha\_1)^{m\_1}\cdots f(\alpha\_n)^{m\_n}\cr
&=&\sum\_{m\_i\geq 0} b\_{m\_1,\dots,m\_n}\alpha\_1^{m\_1}\cdots\alpha\_n^{m\_n}\cr
&=&\alpha,
\end{array}$$

así que $f=\operatorname{id}\_{K}$. En las ecuaciones anteriores hemos usado que $f$ es un homomorfismo de anillos que deja fijo a $F$ y a las raíces $\alpha\_1,\dots,\alpha\_n\in K$ de $p(x)$.
{{% /proof %}}


Un homomorfismo $\varphi$ como el del enunciado se denomina **representación** del grupo de Galois como grupo de permutaciones. Como el homomorfismo $\varphi$ es inyectivo, lo usaremos para identificar $G(K/F)$ con su imagen en $S\_n$. Si cambiamos la numeración de las raíces de $p(x)$ y denotamos $\beta\_i=\alpha\_{\tau(i)}$ para alguna permutación $\tau\in S\_n$, entonces
$$
f(\beta\_i)=f(\alpha\_{\tau(i)})=\alpha\_{\sigma\tau(i)}=\beta_{\tau^{-1}\sigma\tau(i)},
$$
es decir, la permutación asociada a $f$ con respecto a la nueva numeración de las raíces es $\tau^{-1}\sigma\tau$. En particular, los subgrupos de $S\_n$ correspondientes son **conjugados**.

{{% definition %}}
Dado un polinomio separable $p(x)\in F[x]$ de grado $n$, su **grupo de Galois** es el grupo de Galois $G(K/F)$ de un cuerpo de descomposición $K$ de $p(x)$ sobre $F$, visto como subgrupo de $S\_n$ a través del homomorfismo $\varphi$ anterior. Está bien definido salvo conjugación por un elemento de $S\_n$.
{{% /definition %}}

{{% example name="Grupo de Galois de $x^3-2$" %}}
Denotemos por $\alpha=\sqrt[3]{2}$ la raíz cúbica real de $2$ y $\zeta=-\frac{1}{2}+\frac{\sqrt{3}}{2}i$ una raíz cúbica de la unidad. Las raíces de $p(x)=x^3-2$ son entonces $\alpha_1=\alpha,\alpha_2=\alpha\zeta$ y $\alpha_3=\alpha\zeta^2$. El cuerpo de descomposición de $p(x)$ es $K=\mathbb Q(\alpha,\alpha\zeta,\alpha\zeta^2)=\mathbb Q(\alpha,\zeta)$, que tiene grado $6$ sobre $\mathbb Q$ por la fórmula del producto. Cada automorfismo de $G=G(K/\mathbb Q)$ debe llevar $\alpha$ en uno de sus conjugados $\alpha_1,\alpha_2,\alpha_3$, y $\zeta$ en uno de sus conjugados $\zeta,\zeta^2$ (raíces del polinomio mínimo de $\zeta$, $x^2+x+1$). Esto nos da seis opciones en total para la imagen del par $(\alpha,\zeta)$. Como esta imagen determina unívocamente el automorfismo (ya que $\alpha$ y $\zeta$ generan la extensión) y sabemos que hay exactamente seis de ellos, todas las posibles opciones deben darse en algún automorfismo.

Esto nos permite determinar completamente el grupo $G$ de automorfismos de la extensión, a partir de las imágenes de los generadores $\alpha$ y $\zeta$ que, a su vez, nos permiten determinar la imagen de las otras dos raíces de $p(x)$ y expresarlo así en forma de permutación de estas raíces:
|       | $\alpha$ | $\zeta$ | $\alpha\zeta$ | $\alpha\zeta^2$ | permutación |
| --- | --- | --- | --- | --- | --- |
| $\sigma_1$     | $\alpha$      | $\zeta$  | $\alpha\zeta$  |  $\alpha\zeta^2$  | $Id$ |
| $\sigma_2$     | $\alpha$      | $\zeta^2$  | $\alpha\zeta^2$  |  $\alpha\zeta$  | $(23)$ |
| $\sigma_3$     | $\alpha\zeta$      | $\zeta$  | $\alpha\zeta^2$  |  $\alpha$  | $(123)$ |
| $\sigma_4$     | $\alpha\zeta$      | $\zeta^2$  | $\alpha$  |  $\alpha\zeta^2$  | $(12)$ |
| $\sigma_5$     | $\alpha\zeta^2$      | $\zeta$  | $\alpha$  |  $\alpha\zeta$  | $(132)$ |
| $\sigma_6$     | $\alpha\zeta^2$      | $\zeta^2$  | $\alpha\zeta$  |  $\alpha$  | $(13)$ |
{{% /example %}}

## Extensiones ciclotómicas

{{% definition %}}
Una **extensión ciclotómica** es una extensión $F\subseteq K$ generada por un elemento $\zeta\in K$ tal que $\zeta^n=1$ para algún entero $n\geq 1$ (una raíz $n$-ésima de la unidad).
{{% /definition %}}

Supongamos que $n$ es el menor entero positivo tal que $\zeta^n=1$. En tal caso, decimos que $\zeta$ es una **raíz primitiva $n$-ésima** de la unidad.

{{% proposition %}}
Toda extensión ciclotómica $F\subseteq K=F(\zeta)$ es una extensión de Galois. Además, si $\zeta$ es una raíz primitiva $n$-ésima de la unidad, el grupo de Galois $G(K/F)$ es isomorfo a un subgrupo de $(\mathbb Z/(n))^\times$, el grupo multiplicativo de las unidades del anillo $\mathbb Z/(n)$.
{{% /proposition %}}

{{% proof %}}
Notemos que cualquier potencia de $\zeta$ es una raíz del polinomio $x^n-1\in F[x]$. Además, si $\zeta^i=\zeta^j$ con $0\leq i\leq j<n$ tenemos que $\zeta^{j-i}=1$ con $0\leq j-i<n$, así que $i=j$ por la minimalidad de $n$. Por tanto, $1=\zeta^0,\zeta^1,\ldots,\zeta^{n-1}$ son las $n$ raíces del polinomio $x^n-1$, y todas ellas están en $K=F(\zeta)$, así que $K$ es el cuerpo de descomposición de $x^n-1$ sobre $F$. Si $F$ tiene característica $p>0$, entonces $n$ no puede ser múltiplo de $p$: de lo contrario, escribiendo $n=mp$, tendríamos $0=\zeta^n-1=\zeta^{mp}-1=(\zeta^m-1)^p$, por lo que $\zeta^m=1$, lo que contradice la minimalidad de $n$. Concluimos que el polinomio $x^n-1$ es separable, pues su derivada $nx^{n-1}$ no tiene factores comunes con $x^n-1$. Su cuerpo de descomposición es, por tanto, separable.

Como la extensión está generada por $\zeta$, todo automorfismo está unívocamente determinado por la imagen de $\zeta$, que debe ser una raíz primitiva $n$-ésima de la unidad (ya que $\sigma(\zeta)^k=1$ si y solo si $\zeta^k=1$ si $\sigma\in G(K/F)$). En particular, es una raíz del polinomio $x^n-1$, y por tanto de la forma $\zeta^i$ para un único $i\in\\{0,\ldots,n-1\\}$. Además, este $i$ debe ser primo con $n$: de lo contrario, si $d=\operatorname{mcd}(i,n)$, tendríamos que $(\zeta^i)^{n/d}=\zeta^{in/d}=(\zeta^n)^{i/d}=1^{i/d}=1$, por lo que $\zeta^i$ no sería una raíz primitiva $n$-ésima. Podemos definir entonces una aplicación inyectiva
$$\psi\colon G(K/F)\longrightarrow (\mathbb Z/(n))^\times$$
que envía $\sigma$ a la clase $\bar i\in\mathbb Z/(n)$ del único $i\in\\{0,\ldots,n-1\\}$ tal que $\sigma(\zeta)=\zeta^i$. Veamos que es un homomorfismo: si $\sigma,\tau\in G(K/F)$ son tales que $\sigma(\zeta)=\zeta^i$ y $\tau(\zeta)=\zeta^j$ con $i,j\in\\{0,\ldots,n-1\\}$, entonces $\tau(\sigma(\zeta))=\tau(\zeta^i)=\tau(\zeta)^i=(\zeta^j)^i=\zeta^{ij}=\zeta^k$, siendo $k\in\\{0,\ldots,n-1\\}$ el resto de la división de $ij$ entre $n$. Por tanto, $\psi(\tau\sigma)=\bar k=\overline{ij}=\bar i\cdot \bar j=\psi(\tau)\psi(\sigma)$. Como $\psi$ es inyectivo, induce un isomorfismo entre $G(K/F)$ y su imagen en $(\mathbb Z/(n))^\times$.
{{% /proof %}}

{{% corollary %}}
El grupo de Galois de una extensión ciclotómica $F\subseteq K=F(\zeta)$ es abeliano.
{{% /corollary %}}

En el caso particular en el que $n$ es primo, sabemos que $(\mathbb Z/(n))^\times$ es cíclico (por ser el grupo multiplicativo de un cuerpo). Como todo subgrupo de un grupo cíclico es cíclico, obtenemos:

{{% corollary %}}
Si $\zeta$ es una raíz primitiva $p$-ésima de la unidad, donde $p$ es un número primo, el grupo de Galois de la extensión ciclotómica $F\subseteq K=F(\zeta)$ es cíclico.
{{% /corollary %}}

Si $F=\mathbb Q$, $x^p-1=(x-1)(x^{p-1}+x^{p-2}+\cdots+x+1)$ y el polinomio $(x^p-1)/(x-1)=x^{p-1}+x^{p-2}+\cdots+x+1$ es irreducible: mediante el cambio de variable $x\mapsto x+1$ se transforma en $((x+1)^p-1)/x=\sum_{i=1}^p{p\choose i}x^{i-1}$, al cual podemos aplicar el criterio de Eisenstein para el primo $p$ y concluir que es irreducible, por lo que el polinomio original lo es, y es entonces el polinomio mínimo de $\zeta$ sobre $\mathbb Q$. El grado de la extensión $F\subseteq K=F(\zeta)$ es entonces $p-1$, igual al cardinal de $(\mathbb Z/(p))^\times$, por lo que el homomorfismo $\psi$ debe ser biyectivo:

{{% corollary %}}
Sea $p$ un número primo. El grupo de Galois de la extensión ciclotómica $\mathbb Q\subseteq \mathbb Q(\zeta)$, donde $\zeta$ es una raíz primitiva $p$-ésima de la unidad, es isomorfo a $(\mathbb Z/(p))^\times$.
{{% /corollary %}}

Este último resultado es cierto si $\zeta$ es una raíz primitiva $n$-ésima siendo $n$ un entero arbitrario, no necesariamente primo, pero no lo demostraremos aquí.

## Extensiones de Kummer

Dado un cuerpo $F$ de característica distinta de $p$, nuestro objetivo es estudiar el cuerpo de descomposición $K$ del polinomio

$$q(x)=x^p-a\in F[ x ]$$

donde $p$ es primo y $a$ no tiene raíces $p$-ésimas en $F$. Si $\alpha$ es una raíz compleja de $q(x)$, entonces el conjunto de todas sus raíces es

$$\alpha,\zeta\_p\alpha,\dots,\zeta\_p^{p-1}\alpha,$$

donde $\zeta\_p$ es la raíz $p$-ésima primitiva de la unidad, ya que todas son raíces del polinomio $q(x)$ anterior y son todas distintas, pues $\zeta\_p$ tiene orden $p$ para el producto. En particular si $\zeta\_p\in F$ entonces $K=F[\alpha]$.

{{% proposition %}}
Si $\zeta\_p\in F$ y $q(x)=x^p-a\in F[ x ]$ no tiene raíces en $F$ entonces el cuerpo de descomposición $K$ de $q(x)$ tiene grado $p$ sobre $F$. 
{{% /proposition %}}


{{% proof %}}
Sea $\alpha$ una raíz compleja de $q(x)$. Hemos observado que $K=F[\alpha]$ y $\alpha$ es una raíz de $q(x)$, que es de grado $p$, por tanto $[K:F]\leq p$. Al ser $F\subset K$ de Galois, para probar la otra desigualdad bastará ver que $[K:F]=|G(K/F)|\geq p$. 

Como $\alpha\notin F= K^{G(K/F)}$, ha de existir algún $f\in G(K/F)$ tal que $f(\alpha)\neq\alpha$. Como $f$ preserva raíces de polinomios en $F[ x ]$, $f(\alpha)=\zeta\_p^i\alpha$ para cierto $0<i<p$. Usaremos esto para ver que las potencias $f^j$ de $f$ son diferentes para todo $0\leq j<p$, así que $G(K/F)$ tendrá en efecto al menos $p$ elementos. Para ello basta comprobar que cada una de estas potencias $f^j$ manda $\alpha$ a un elemento diferente. Vamos a probar por inducción que

$$f^j(\alpha)=(\zeta\_p^{i})^{j}\alpha.$$

Todos estos elementos son diferentes ya que al ser $p$ primo todas las potencias de $\zeta\_p$ distintas de $1$, por ejempo $\zeta\_p^i$, tienen orden multiplicativo $p$, así que todos los $(\zeta\_p^i)^{j}$ son diferentes para $0\leq j<p$. Para $j=1$ la ecuación anterior es obviamente cierta. Supongamos que es cierta para $j-1$. Como $\zeta\_p\in F$ se tiene que $f(\zeta\_p)=\zeta\_p$. Por tanto,

$$\begin{array}{rcl}
f^j(\alpha)&=&f(f^{j-1}(\alpha))\cr
&=&f((\zeta\_p^{i})^{j-1}\alpha)\cr
&=&(f(\zeta\_p)^{i})^{j-1}f(\alpha)\cr
&=&(\zeta\_p^{i})^{j-1}\zeta\_p^i\alpha\cr
&=&(\zeta\_p^{i})^{j}\alpha.
\end{array}$$

{{% /proof %}}

{{% remark %}}
A posteriori vemos que, en las condiciones de la proposición anterior, $x^p-a$ es un polinomio irreducible, pues cualquiera de sus raíces complejas tiene grado $p$.
{{% /remark %}}


Sorprendentemente el resultado anterior tiene un recíproco.

{{% theorem %}}
Si $p$ es un primo, $F$ es un cuerpo tal que $\zeta\_p\in F$ y $F\subset K$ es una extensión de Galois de grado $[K:F]=p$ entonces $K=F[\alpha]$ para cierto $\alpha\in K$ que es raíz de un polinomio de la forma $x^p-a\in F[ x ]$. 
{{% /theorem %}}


{{% proof %}}
Al ser la extensión de Galois $|G(K/F)|=[K:F]=p$, el grupo $G(K/F)$ es cíclico de orden $p$, así que cualquier $f\in G(K/F)$ distinto de la identidad genera el grupo de Galois,

$$G(K/F)=\\{f^0,f^1,\dots, f^{p-1}\\}.$$

Sea $\alpha\in K\backslash F$ y, para cada $i=1,\ldots,p-1$, denotemos $\beta_i=\sum_{j=0}^{p-1}\zeta\_p^{-ij}f^j(\alpha)=\alpha+\zeta\_p^{-i}f(\alpha)+\zeta\_p^{-2i}f^2(\alpha)+\cdots+\zeta\_p^{-(p-1)i}f^{p-1}(\alpha)$. Veamos que algún $\beta_i$ debe ser distinto de cero. De lo contrario, tendríamos que
$$
0=\sum_{i=1}^{p-1}\beta_i=\sum_{i=1}^{p-1}\sum_{j=0}^{p-1}\zeta_p^{-ij}f^j(\alpha)=\sum_{j=0}^{p-1}f^j(\alpha)\sum_{i=1}^{p-1}(\zeta_p^{-j})^i=p\alpha-\sum_{j=0}^{p-1}f^j(\alpha)
$$
ya que, para $j\neq 0$, $\sum_{i=1}^{p-1}(\zeta_p^{-j})^i=-1$ por ser $\zeta_p^{-j}$ raíz del polinomio $x^{p-1}+\cdots+x+1$. Deducimos que $p\alpha=\sum_{j=0}^{p-1}f^j(\alpha)$. Pero esto es imposible, ya que el término de la izquierda no está en $F$ y el de la derecha sí lo está (puesto que es fijo por $f$).

Aplicando $f$ a $\beta_i$, obtenemos
$$
f(\beta_i)=f\left(\sum_{j=0}^{p-1}\zeta\_p^{-ij}f^j(\alpha)\right)=\sum_{j=0}^{p-1}\zeta\_p^{-ij}f^{j+1}(\alpha)=\zeta_p^i\sum_{j=0}^{p-1}\zeta\_p^{-i(j+1)}f^{j+1}(\alpha)=\zeta_p^i\cdot\beta_i,
$$
por lo que $f(\beta_i^p)=(\zeta_p^i\cdot\beta_i)^p=\beta_i^p$, esto es, $\beta_i^p\in F$ y $\beta_i$ es raíz del polinomio $x^p-\beta_i^p\in F[x]$. Además, si $\beta_i\neq 0$, entonces $f(\beta_i)=\zeta_p^i\cdot\beta_i\neq\beta_i$, por lo que $\beta_i\not\in F$. Como el grado de la extensión $F\subseteq K$ es primo, $\beta_i$ debe generar la extensión completa $K$.
{{% /proof %}}

Igual que antes, en las condiciones del enunciado de este teorema el polinomio $x^p-a$ es necesariamente irreducible.

Las extensiones del tipo que hemos estudiado en esta sección se denominan **extensiones de Kummer**.


## Solubilidad por radicales

{{% definition %}}
Decimos que $\alpha\in \mathbb{C}$ es **soluble** sobre un cuerpo $F$ si existe una cadena de extensiones

$$F = F\_0\subset F\_1\subset F\_2\subset\cdots\subset  F\_n=K$$

tal que $\alpha\in K$ y $F\_{i+1}=F\_i[\sqrt[s\_i]{r\_i}]$ para ciertos $r\_i\in F\_i$ y $s\_i\geq 2$, $0\leq i<n$.
{{% /definition %}}

Los números solubles sobre $F$ son los que se obtienen a partir de números de $F$ realizando iteradamente sumas, restas, productos, divisiones por números no nulos y raíces $n$-ésimas. Nuestro objetivo es saber cuándo podemos hallar las raíces de un polinomio $p(x)\in F[ x ]$ de este modo a partir de sus coeficientes, es decir, queremos saber cuándo las raíces de $p(x)$ son solubles sobre $F$. Veremos cómo hacerlo usando el grupo de Galois del cuerpo de descomposición de $p(x)$.

{{% remark %}}
Como $\sqrt[st]{r}=\sqrt[s]{\sqrt[t]{r}}$, no hay pérdida de generalidad si en la definición anterior suponemos que los $s\_i$ son todos primos.

Añadiendo las raíces de manera sucesiva vemos que si $\alpha\_1,\dots,\alpha\_m\in\mathbb{C}$ son solubles entonces existe una cadena de extensiones como la de la definición tal que $\alpha\_1,\dots,\alpha\_m\in K$.
{{% /remark %}}



{{% definition %}}
Un grupo $G$ es **soluble** si existe una cadena de subgrupos

$$\\{e\\}=G\_0 \subset G\_1 \subset G\_2 \subset \cdots\subset G\_n=G$$

tal que $G\_i\subset G\_{i+1}$ es un subgrupo normal con cociente $G\_{i+1}/G\_i$ abeliano para todo $0\leq i<n$.
{{% /definition %}}

La solubilidad es una buena propiedad porque permite probar por inducción que muchas propiedades de los grupos abelianos son también ciertas para los grupos solubles.

{{% remark %}}
Los grupos abelianos son solubles. Los grupos simétricos $S\_2$, $S\_3$ y $S\_4$ también, así como todos sus subgrupos. Sin embargo, $S\_n$ no es soluble para ningún $n\geq 5$, ni tampoco su subgrupo alternado $A\_n\subset S\_n$. La solubilidad se preserva por isomorfismos.
{{% /remark %}}

{{% lemma %}}
Dado un grupo $G$ y un subgrupo normal $N$, $G$ es soluble si y solo si lo son $N$ y $G/N$.
{{% /lemma %}}

{{% proof %}}
Denotamos $p\colon G\twoheadrightarrow G/ N$ a la proyección natural.

$\Rightarrow$ Si 

$$\\{e\\}=G\_0 \subset G\_1 \subset G\_2 \subset \cdots\subset G\_n=G$$

es una cadena en las condiciones de la definición entonces las cadenas siguientes demuestran que $N$ y $G/N$ son solubles,

$$
\begin{array}{c}
\\{e\\}=N\cap G\_0 \subset N\cap G\_1 \subset \cdots\subset N\cap G\_n=N,\cr
\\{e\\}=p(G\_0) \subset p(G\_1) \subset \cdots\subset p(G\_n)=G/N.
\end{array}
$$

Aquí usamos que, gracias al primer teorema de isomorfía,

$$
\frac{N\cap G\_{i+1}}{N\cap G\_{i}}\subset\frac{G\_{i+1}}{G\_i}
\cong\frac{p(G\_{i+1})}{p(G\_{i})}.
$$

$\Leftarrow$ Si $N$ y $G/N$ son solubles gracias a las cadenas

$$
\begin{array}{c}
\\{e\\}=N\_0 \subset N\_1 \subset N\_2 \subset \cdots\subset N\_m=N,\cr
\\{e\\}=K\_0 \subset K\_1 \subset K\_2 \subset \cdots\subset K\_n=G/N,
\end{array}
$$

entonces $G$ es soluble gracias a la cadena

$$
\\{e\\}=N\_0 \subset \cdots\subset N\_m=p^{-1}(K\_0)\subset \cdots\subset p^{-1}(K\_m)=G.
$$

Aquí usamos que, gracias al primer teorema de isomorfía,

$$
\frac{p^{-1}(K\_{i+1})}{p^{-1}(K\_{i})}\cong \frac{K\_{i+1}}{K\_i}.
$$

{{% /proof %}}

{{% corollary %}}
Dos grupos $G$ y $H$ son solubles si y solo si $G\times H$ es soluble.
{{% /corollary %}}

{{% proof %}}
Basta usar el primer teorema de isomorfía para observar que $G\cong G\times \\{e\\}\subset G\times H$ es un subgrupo normal y $(G\times H)/(G\times \\{e\\})\cong H$.
{{% /proof %}}


{{% proposition %}}
Un grupo finito $G$ es soluble si y solo si existe una cadena de subgrupos

$$\\{e\\}=G\_0 \subset G\_1 \subset G\_2 \subset \cdots\subset G\_n=G$$

tal que $G\_i\subset G\_{i+1}$ es un subgrupo normal con cociente $G\_{i+1}/G\_i$ de orden primo, $0\leq i<n$.
{{% /proposition %}}

{{% proof %}}
Antes que nada, observamos que la demostración del lema anterior también sirve para probar que si $G$ es un grupo y $N\subset G$ es un subgrupo normal, entonces $G$ satisface la condición del enunciado de esta proposición si y solo si $N$ y $G/N$ la cumplen. En particular, dos grupos $G$ y $H$ la satisfacen si y solo si el producto $G\times H$ la cumple. Partiendo de esto, abordamos ahora la prueba de esta proposición.

$\Leftarrow$ Es obvio porque todo grupo de orden primo es cíclico y por tanto abeliano.

$\Rightarrow$ Si $G=\mathbb{Z}/(p^n)$ basta tomar $G\_i=(\bar{p}^{n-i})$, $0\leq i<n$, ya que todos los subgrupos de $G$ son normales por ser abelianos y  $\bar{p}^{n-i}\in\mathbb{Z}/(p^n)$ tiene orden $p^i$, así que $|G\_i|=p^i$ y por tanto 

$$
\begin{array}{rcl}
\left|\frac{G\_{i+1}}{G\_i}\right|&=&\frac{|G\_{i+1}|}{|G\_i|}\cr
&=&\frac{p^{i+1}}{p^i}\cr
&=&p.
\end{array}
$$

Si $G$ es abeliano el resultado también es cierto, ya que al ser finito sería un producto finito de grupos de la forma $\mathbb{Z}/(p^n)$, en virtud del segundo teorema de estructura.

En general, si $G$ satisface la condición de solubilidad gracias a la cadena

$$
\\{e\\}=G\_0 \subset G\_1 \subset G\_2 \subset \cdots\subset G\_n=G,
$$

vamos a probar por inducción que cada $G\_i$ satisface la condición del enunciado. Obviamente $G\_0$ la satisface por ser trivial. Si $G\_i$ la cumple, como $G\_i\subset G\_{i+1}$ es normal y $G\_{i+1}/G\_i$ la satisface por ser abeliano, tenemos que $G\_{i+1}$ también la cumple.
{{% /proof %}}

{{% lemma %}}
Dados dos polinomios $f\_1,f\_2\in F[ x ]$, si $L\_1$ y $L\_2$ son los cuerpos de descomposición de $f\_1$ y $f\_2$, respectivamente, y $K$ es el cuerpo de descomposición de $f\_1f\_2$ entonces $G(K/F)$ es isomorfo a un subgrupo de $G(L\_1/F)\times G(L\_2/F)$.
{{% /lemma %}}

{{% proof %}}
Tenemos que $F\subset L\_1,L\_2\subset K$, ya que las raíces de un producto de dos polinomios son la unión de las raíces de los factores. Consideramos el homomorfismo

$$G(K/F)\longrightarrow\frac{G(K/F)}{G(K/L\_1)}\times \frac{G(K/F)}{G(K/L\_2)}\cong G(L\_1/F)\times G(L\_2/F)$$

definido en cada coordenada como la proyección natural. El núcleo es $G(K/L\_1)\cap G(K/L\_2)$, es decir, los automorfismos de $K$ que dejan fijas a las raíces tanto de $f\_1$ como de $f\_2$. Un automorfismo así deja fijas a las raíces de $f\_1f\_2$ y por tanto a su cuerpo de descomposición $K$, así que tiene que ser la identidad. Como el núcleo es trivial, el homomorfismo es inyectivo y, en virtud del primer teorema de isomorfía, el dominio es isomorfo a un subgrupo del codominio.
{{% /proof %}}

{{% lemma %}}
Si $p\_1,\dots,p\_m$ son enteros primos entonces el grupo de Galois de la extensión de Galois $F\subset F[\zeta\_{p\_1},\dots,\zeta\_{p\_m}]$ es abeliano.
{{% /lemma %}}

{{% proof %}}
Para $m=1$ se prueba como en el caso $F=\mathbb{Q}$. Por inducción, si es cierto para $m-1$ primos, nuestro grupo de Galois es isomorfo a un subgrupo del producto de los de $F\subset F[\zeta\_{p\_1},\dots,\zeta\_{p\_{m-1}}]$ y $F\subset F[\zeta\_{p\_m}]$ en virtud del lema anterior. El producto de grupos abelianos es abeliano y los subgrupos de los grupos abelianos también.
{{% /proof %}}


{{% theorem %}}
Sea $p(x)\in F[ x ]$ un polinomio con cuerpo de descomposición $L$. Las raíces complejas de $p(x)$ son todas solubles sobre $F$ si y solo si $G(L/F)$ es un grupo soluble.
{{% /theorem %}}

{{% proof %}}

$\Leftarrow$ Denotamos $G=G(L/F)$. Sea

$$\\{e\\}=G\_0 \subset G\_1 \subset G\_2 \subset \cdots\subset G\_n=G$$

una cadena de subgrupos como en la proposición anterior. 

Supongamos primero que $F$ tiene todas las raíces primitivas de la unidad asociadas a los primos que aparecen como el orden de los cocientes $G\_{i+1}/G\_i$. En este caso basta considerar la cadena de extensiones

$$L= L^{G\_0} \supset L^{G\_1} \supset \cdots\supset L^{G\_n}= F[\zeta\_{p\_1},\dots, \zeta\_{p\_n}]= F.$$

En efecto, el teorema sobre extensiones de Kummer garantiza que cada $L^{G\_{i}}\supset L^{G\_{i+1}}$ se obtiene añadiendo una raíz.

Si $F$ no tuviera todas las raíces primitivas de la unidad mencionadas, denotamos $F'$ y $L'$ a los cuerpos obtenidos al añadírselas a $F$ y a $L$, respectivamente. Por construcción, la extensión $F\subset F'$ se puede interpolar como en la definición de número soluble. El grupo $G(L'/L)$ es abeliano por el lema anterior y

$$\frac{G(L'/F)}{G(L'/L)}\cong G(L/F),$$

que es soluble, así que $G(L'/F)$ es soluble y su subgrupo normal $G(L'/F')$ también. Como $F'$ posee todas las raíces primitivas de la unidad necesarias, el párrafo anterior demuestra que $F'\subset L'$ también se puede interpolar como en la definición de número soluble. Por tanto $F\subset L'$ también, concatenando ambas interpolaciones.

$\Rightarrow$ El argumento es muy parecido anterior. Lo dejamos como ejercicio.
{{% /proof %}}


